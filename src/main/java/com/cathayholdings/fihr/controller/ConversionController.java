package com.cathayholdings.fihr.controller;

import com.cathayholdings.fihr.service.CdaConversionService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class ConversionController {

  private final CdaConversionService cdaConversionService;

  @PostMapping(value = "/api/convert", consumes = MediaType.APPLICATION_XML_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<String> convertCdaToFhir(@RequestBody String cdaXML) throws Exception {

    String fhir = cdaConversionService.convertCdaXMLStringToFhirJson(cdaXML);

    return ResponseEntity.ok(fhir);
  }
}
