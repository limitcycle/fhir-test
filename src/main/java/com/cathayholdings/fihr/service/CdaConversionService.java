package com.cathayholdings.fihr.service;

import ca.uhn.fhir.parser.IParser;
import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.Identifier;
import org.openhealthtools.mdht.uml.cda.consol.ConsolPackage;
import org.openhealthtools.mdht.uml.cda.consol.ContinuityOfCareDocument;
import org.openhealthtools.mdht.uml.cda.util.CDAUtil;
import org.springframework.stereotype.Service;
import tr.com.srdc.cda2fhir.conf.Config;
import tr.com.srdc.cda2fhir.transform.CCDTransformerImpl;
import tr.com.srdc.cda2fhir.transform.ICDATransformer;
import tr.com.srdc.cda2fhir.util.IdGeneratorEnum;

@Service
public class CdaConversionService {

  public String convertCdaXMLStringToFhirJson(String cdaXML) throws Exception {
    ContinuityOfCareDocument ccd = (ContinuityOfCareDocument) CDAUtil.loadAs(
        new ByteArrayInputStream(cdaXML.getBytes(StandardCharsets.UTF_8)),
        ConsolPackage.eINSTANCE.getContinuityOfCareDocument());

    ICDATransformer ccdTransformer = new CCDTransformerImpl(IdGeneratorEnum.COUNTER);
    Config.setGenerateDafProfileMetadata(true);

    Identifier identifier = new Identifier();
    identifier.setValue("");

    Bundle bundle = ccdTransformer.transformDocument(ccd, cdaXML, identifier);
    IParser jsonParser = Config.getFhirContext().newJsonParser();
    return jsonParser.encodeResourceToString(bundle);
  }

}
